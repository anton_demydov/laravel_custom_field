export default {
    data(){
        return{
            items: []
        }
    },

    created: function()
    {
        this.fetchItems();
    },

    methods: {
        fetchItems()
        {
            let uri = '/items';
            this.axios.get(uri).then((response) => {
                this.items = response.data;
            });
        },
        deleteItem(id)
        {
            let uri = `/items/${id}`;
            this.items.splice(id, 1);
            this.axios.delete(uri);
        }
    }
}