export default{
    data(){
        return{
            item:{}
        }
    },

    created: function(){
        this.getItem();
    },

    methods: {
        getItem()
        {
            let uri = `/items/${this.$route.params.id}/edit`;
            this.axios.get(uri).then((response) => {
                this.item = response.data;
            });
        },

        updateItem()
        {
            let uri = '/items/'+this.$route.params.id;
            this.axios.patch(uri, this.item).then((response) => {
                this.$router.push({name: 'DisplayItem'});
            });
        }
    }
}