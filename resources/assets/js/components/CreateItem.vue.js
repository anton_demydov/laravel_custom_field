export default {
    data(){
        return{
            item:{}
        }
    },
    methods: {
        addItem(){
            let uri = '/items';
            this.axios.post(uri, this.item).then((response) => {
                this.$router.push({name: 'DisplayItem'})
            })
        }
    }
}