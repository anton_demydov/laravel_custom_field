<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <script>
        function addLine() {
            var e = document.querySelector('#addLine');

            var node = document.createElement("div");
            var myinnerHTML = '<div class="col-xs-6">' +
                    '<div class="form-group">' +
                    '<label for="custom_fields['+(last_line+1)+'][name]">New Field Title </label> ' +
                    '<input name="custom_fields['+(last_line+1)+'][name]" type="text" value=""  class="form-control">' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-xs-6"><div class="form-group">' +
                    '<label for="custom_fields['+(last_line+1)+'][value]">New Field Value </label> ' +
                    '<input name="custom_fields['+(last_line+1)+'][value]" type="text" value=""  class="form-control">' +
                    '</div>' +
                    '</div>';
            node.innerHTML = myinnerHTML;
            e.before(node);
            window.last_line++;
        }

        function ready() {
            window.last_line  = parseInt(document.querySelector('#addLine').dataset.line);
            var element = document.querySelector('#addLine');
            element.addEventListener('click', addLine);
        }

        document.addEventListener("DOMContentLoaded", addLine);
        document.addEventListener("DOMContentLoaded", ready);

    </script>
</body>
</html>
