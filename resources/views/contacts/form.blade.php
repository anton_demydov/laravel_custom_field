@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if($contact)
{!!  Form::open(['url' => URL::route('contacts.update',['id'=>$contact->id])]) !!}
@else
{!!  Form::open(['url' => URL::route('contacts.create')]) !!}
@endif
{{csrf_field()}}
{{method_field('put')}}

<div class="col-xs-12 col-md-6">
    <div class="form-group">
        {{ Form::label('first_name', 'First name') }}
        {{ Form::text('first_name', old('first_name',$contact->first_name), ['class'=>'form-control']) }}
    </div>
</div>

<div class="col-xs-12 col-md-6">
    <div class="form-group">
        {{ Form::label('last_name', 'Last name') }}
        {{ Form::text('last_name', old('last_name',$contact->last_name), ['class'=>'form-control']) }}
    </div>
</div>

<div class="col-xs-12 col-md-6">
    <div class="form-group">
        {{ Form::label('address', 'Address') }}
        {{ Form::text('address', old('address',$contact->address), ['class'=>'form-control']) }}
    </div>
</div>


<div class="col-xs-12 col-md-6">
    <div class="form-group">
        {{ Form::label('phone', 'Phone') }}
        {{ Form::text('phone', old('phone',$contact->phone), ['class'=>'form-control']) }}
    </div>
</div>

<div class="col-xs-12">
    <div class="form-group">
        {{ Form::label('email', 'Email') }}
        {{ Form::text('email', old('email',$contact->email), ['class'=>'form-control']) }}
    </div>
</div>
<div id="extra_fields">
    @if($contact->customFields()->count())

        @foreach($contact->customFields as  $custom_field)
            <div class="col-xs-6">
                <div class="form-group">
                    {{ Form::label('custom_fields['.$loop->index.'][name]', 'New Field Title ') }}
                    {{ Form::text('custom_fields['.$loop->index.'][name]', $custom_field->name, ['class'=>'form-control']) }}
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    {{ Form::label('custom_fields['.$loop->index.'][value]', 'New Field Value ') }}
                    {{ Form::text('custom_fields['.$loop->index.'][value]', $custom_field->value, ['class'=>'form-control']) }}
                </div>
            </div>
            <?php $line = $loop->index?>

        @endforeach
    @else
        <?php $line = 0?>
    @endif
    <div class="empty_ell"></div>
</div>
<div class="col-xs-12 ">
    <div class="form-group">
        <div class="form-group">
            <a class=" btn btn-default form-control" id="addLine" data-line="{{$line}}"> Add new field</a>
        </div>
    </div>

</div>
    <div class="col-xs-12 ">
        <div class="form-group">
            {!! Form::submit('Submit', ['class'=>'form-control']) !!}
        </div>
    </div>
{!! Form::close() !!}