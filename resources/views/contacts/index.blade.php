@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Contacts</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                            <div class="row">
                                <div class="col-xs-12">
                                    <a class="btn btn-success"
                                       href="{{route('contacts.create')}}">
                                        <i class="glyphicon glyphicon-plus"></i> Add Contact
                                    </a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Controls</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($user->contacts as $contact)
                                            <tr>
                                                <td>
                                                    {{$contact->first_name}} {{$contact->last_name}}
                                                </td>
                                                <td>
                                                    {{$contact->email}}
                                                </td>
                                                <td>
                                                    <a href="{{route('contacts.edit',['id'=>$contact->id])}}" class="btn btn-warning">Edit</a>
                                                    {{Form::open(['url'=>route('contacts.destroy',['id'=>$contact->id])])}}
                                                    {{method_field('delete')}}
                                                    <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')">Delete</button>
                                                    {{Form::close()}}

                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
