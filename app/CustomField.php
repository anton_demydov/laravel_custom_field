<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomField extends Model
{

    protected $fillable = ['name', 'value'];

    public function contact(){
        return $this->belongsTo(Contact::class);
    }

}
