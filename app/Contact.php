<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = ['first_name', 'last_name', 'email', 'phone', 'address','custom_fields'];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function customFields(){
        return $this->hasMany(CustomField::class);
    }

    public function setCustomFieldsAttribute($custom_fields){


        if($custom_fields){

            $custom_fields = array_filter($custom_fields['custom_fields'],function($data){ return isset($data['name'])?true:false;});

            $this->customFields()->delete();
            $this->customFields()->createMany($custom_fields);
        }
    }

}
