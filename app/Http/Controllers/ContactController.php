<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user = Auth::user();
        if (View::exists('contacts.index')) {
            return view('contacts.index', compact('user'));
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        $contact = new Contact;
        if (View::exists('contacts.create')) {
            return view('contacts.create', compact('user', 'contact'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        if (!$this->validate($request, [
            'first_name' => 'required|max:255',
            'last_name' => 'max:255',
            'address' => 'max:255',
            'phone' => 'max:255',
            'email' => 'max:255|email',
            'custom_fields' => ''
        ])
        ) {
            return Redirect::route('contacts.create')
                ->withInput($request->all());
        } else {
            $input = $request->only([
                'first_name',
                'last_name',
                'phone',
                'email',
                'address',
            ]);
            $contact = Auth::user()->contacts()->create($input);
            return Redirect::route('contacts.show',['id'=>$contact->id]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();
        $contact = Contact::where(['id'=>$id, 'user_id'=>$user->id])->firstOrFail();
        if (View::exists('contacts.show')) {
            return view('contacts.show', compact('user', 'contact'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $contact = Contact::with('customFields')->where(['id'=>$id, 'user_id'=>$user->id])->firstOrFail();

        if (View::exists('contacts.edit')) {
            return view('contacts.edit', compact('user', 'contact'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!$this->validate($request, [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'address' => 'required|max:255',
            'phone' => 'required|max:255',
            'email' => 'required|max:255|email',
            'custom_fields' => ''
        ])
        ) {
            return Redirect::route('contacts.create')
                ->withInput($request->all());
        } else {



            $input = $request->only('first_name', 'last_name', 'email', 'phone', 'address');

            $custom_fields = $request->only('custom_fields');

            $user = Auth::user();

            $contact = Contact::where(['id'=>$id, 'user_id'=>$user->id])->firstOrFail();

            $contact->custom_fields = $custom_fields;

            $contact->update($input);

            return Redirect::route('contacts.show',['id'=>$contact->id]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();

        $contact = Contact::where(['id'=>$id, 'user_id'=>$user->id])->firstOrFail();
        $contact->customFields()->delete();
        $contact->delete();
            return Redirect::route('contacts.index');

    }
}
